from env import request, cacheRequest, cacheRequest, ignoredRequest, extract
from java.util.regex import Pattern

class Project:
    
    def __init__(self, testIndex, projectKey, projectId):
        self.projectKey = projectKey
        self.projectId = projectId
        cacheIndex = testIndex * 100
        self.requests = {
            'view' : request(testIndex, 'HTTP-REQ : view project'),
            'activity_stream' : request(testIndex + 1, 'HTTP-REQ : view projects activity stream'),
            'view_cache' : cacheRequest(cacheIndex, 'CACHE : view projects'),
            'ignored'  : ignoredRequest(),
        }
        self.patterns = {
            'chart' : Pattern.compile('<img src=\'(\/charts\?filename=.*?)\''),
            'streams' : Pattern.compile('id="gadget-1.*?src="http:\/\/.*?(\/.*?)"'),
            'project_avatar' : Pattern.compile('id="project-avatar".*?src="(.*?)"')
        }
        
    def getProjectKey(self):
        return self.projectKey
    
    def getProjectId(self):
        return self.projectId
        
    def view(self, cached=False):
        req = self.requests['view']
        cacheReq = self.requests['view_cache']
        activityReq = self.requests['activity_stream']
        ignoredReq = self.requests['ignored']
        
        page = req.GET('/browse/' + self.projectKey).text
        
        ignoredReq.GET(extract(page, self.patterns['chart']))
        ignoredReq.GET(extract(page, self.patterns['streams']))
        ignoredReq.GET('/rest/activity-stream/1.0/preferences')
        req.GET('/plugins/servlet/streams?maxResults=10&streams=key+IS+' + self.projectKey)

        if not cached:        
            cacheReq.GET('/s/en_US-4qg4om/732/5/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/css/atl.general/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/js/atl.general/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:browseproject/jira.webresources:browseproject.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cacheReq.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js')
            cacheReq.GET('/images/icons/newfeature.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            cacheReq.GET('/images/icons/bug.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/ico_reports.png')
            cacheReq.GET('/secure/projectavatar?pid=10000&avatarId=10011&size=large')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/tools_20.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/ico_filters.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/mod_header_bg.png')
            cacheReq.GET('/images/icons/box_16.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            cacheReq.GET('/plugins/servlet/gadgets/js/auth-refresh.js?v=61901225c398ca5626e5f70307fcebd&container=atlassian&debug=0')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.css')
            cacheReq.GET('/s/en_USkppxta/712/13/5.1.0/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-compatibility/com.atlassian.auiplugin:jquery-compatibility.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-lib/com.atlassian.auiplugin:jquery-lib.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:aui-core/com.atlassian.auiplugin:aui-core.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:templates/com.atlassian.gadgets.publisher:templates.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:util/com.atlassian.gadgets.publisher:util.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:date-default/com.atlassian.streams:date-default.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:date-js/com.atlassian.streams:date-js.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:streams-parent-js/com.atlassian.streams:streams-parent-js.js')
            cacheReq.GET('/s/en_USkppxta/712/13/5.1.0/_/download/batch/com.atlassian.streams.actions:inlineActionsJs/com.atlassian.streams.actions:inlineActionsJs.js')
            cacheReq.GET('/s/en_USkppxta/712/13/5.1.0/_/download/batch/com.atlassian.streams.actions:inlineActionsJs/com.atlassian.streams.actions:inlineActionsJs.js')
            cacheReq.GET('/s/en_USkppxta/712/13/5.1.0/_/download/batch/com.atlassian.streams.jira.inlineactions:actionHandlers/com.atlassian.streams.jira.inlineactions:actionHandlers.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:streamsGadgetResources/com.atlassian.streams:streamsGadgetResources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/feed-icon.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/resources/com.atlassian.gadgets.publisher:ajs-gadgets/images/tools_12.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/resources/com.atlassian.gadgets.publisher:ajs-gadgets/images/menu_indicator_for_light_backgrounds.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/resources/com.atlassian.streams.streams-jira-plugin:date-en-US/date.js?callback=ActivityStreams.loadDateJs&_=1328668289887')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/gadget-loading.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/throbber.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/resources/jira.webresources:global-static/wiki-renderer.css')

    def viewTabVersions(self, cached=False):
        req = self.requests['view']
        cacheReq = self.requests['view_cache']
        
        page = req.GET('/browse/' + self.projectKey + '#selectedTab=com.atlassian.jira.plugin.system.project%3Aversions-panel').text
        
        if not cached:
            cacheReq.GET('/s/en_US-4qg4om/732/5/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/css/atl.general/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cacheReq.GET('/s/en_US-4qg4om/732/5/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/js/atl.general/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:browseproject/jira.webresources:browseproject.js')
            cacheReq.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            cacheReq.GET('/images/icons/newfeature.gif')
            cacheReq.GET('/images/icons/bug.gif')
            cacheReq.GET('/images/icons/box_16.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            cacheReq.GET(extract(page, self.patterns['project_avatar']))


    def viewTabIssues(self, cached=False):
        req = self.requests['view']
        cacheReq = self.requests['view_cache']     
        page = req.GET('/browse/' + self.projectKey + '#selectedTab=com.atlassian.jira.plugin.system.project%3Aissues-panel').text
        
        if not cached:
            cacheReq.GET('/s/en_US-4qg4om/732/5/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/css/atl.general/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cacheReq.GET('/s/en_US-4qg4om/732/5/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/js/atl.general/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:browseproject/jira.webresources:browseproject.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cacheReq.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js')
            cacheReq.GET('/images/icons/newfeature.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            cacheReq.GET('/images/icons/bug.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/mod_header_bg.png')
            cacheReq.GET('/images/icons/priority_major.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            cacheReq.GET('/images/icons/status_open.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            cacheReq.GET(extract(page, self.patterns['project_avatar']))            


    def viewTabComponents(self, cached=False):
        req = self.requests['view']
        cacheReq = self.requests['view_cache']
        page = req.GET('/browse/' + self.projectKey + '#selectedTab=com.atlassian.jira.plugin.system.project%3Acomponents-panel').text
        
        if not cached:
            cacheReq.GET('/s/en_US-4qg4om/732/5/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/css/atl.general/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/js/atl.general/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-standalone-resources/com.atlassian.gadgets.embedded:gadget-standalone-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:browseproject/jira.webresources:browseproject.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cacheReq.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js')
            cacheReq.GET('/images/icons/bug.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            cacheReq.GET('/images/icons/newfeature.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/add_20.png')
            cacheReq.GET('/images/icons/component.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            cacheReq.GET(extract(page, self.patterns['project_avatar']))           
